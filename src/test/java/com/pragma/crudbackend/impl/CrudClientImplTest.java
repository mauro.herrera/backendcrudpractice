package com.pragma.crudbackend.impl;

import com.pragma.crudbackend.dto.Client;
import com.pragma.crudbackend.dto.ClientImage;
import com.pragma.crudbackend.dto.ClientPrimaryKey;
import com.pragma.crudbackend.dto.ClientServiceObject;
import com.pragma.crudbackend.exceptions.ClientNotFoundException;
import com.pragma.crudbackend.exceptions.NoDataException;
import com.pragma.crudbackend.interfaces.ICrudClientImageRepository;
import com.pragma.crudbackend.interfaces.ICrudClientMapper;
import com.pragma.crudbackend.interfaces.ICrudClientRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = {CrudClientImpl.class})
class CrudClientImplTest {


    @Autowired
    CrudClientImpl crudClientImpl;

    @MockBean
    private ICrudClientRepository crudClientRepository;

    @MockBean
    private ICrudClientImageRepository crudClientImageRepository;

    @MockBean
    private ICrudClientMapper crudClientMapper;

    ClientServiceObject clientServiceObject = createClientServiceObject();

    @Test
    void createClientTest() {
        when(crudClientMapper.mapServiceObjectToClient(any(ClientServiceObject.class))).thenReturn(new Client());
        when(crudClientMapper.mapServiceObjectToClientImage(any(ClientServiceObject.class))).thenReturn(new ClientImage());
        when(crudClientRepository.save(any(Client.class))).thenReturn(new Client());
        when(crudClientImageRepository.save(any(ClientImage.class))).thenReturn(new ClientImage());

        assertEquals(clientServiceObject, crudClientImpl.createClient(clientServiceObject));
    }

    @Test
    void getClientByIdTest() {
        when(crudClientRepository.findById(any(ClientPrimaryKey.class))).thenReturn(Optional.of(buildClient()));
        when(crudClientImageRepository.findById(anyString())).thenReturn(Optional.of(buildClientImage()));
        when(crudClientMapper.mapClientToServiceObject(any(Client.class), any(ClientImage.class))).thenReturn(clientServiceObject);

        assertEquals(clientServiceObject, crudClientImpl.getClientById("CED", "123456789"));
    }

    @Test
    void getClientByIdFailTest() {
        when(crudClientRepository.findById(any(ClientPrimaryKey.class))).thenReturn(Optional.empty());
        when(crudClientImageRepository.findById(anyString())).thenReturn(Optional.empty());

        assertThrows(ClientNotFoundException.class, () -> crudClientImpl.getClientById("CED", "123456789"));
    }

    @Test
    void getAllClientsTest() {
        when(crudClientRepository.findAll()).thenReturn(List.of(buildClient()));
        when(crudClientImageRepository.findAll()).thenReturn(List.of(buildClientImage()));
        when(crudClientMapper.mapClientListToServiceObjectList(any(), any())).thenReturn(List.of(clientServiceObject));

        assertEquals(List.of(clientServiceObject), crudClientImpl.getAllClients());
    }

    @Test
    void updateClientTest() {
        when(crudClientRepository.getById(any(ClientPrimaryKey.class))).thenReturn(buildClient());
        when(crudClientImageRepository.findById(anyString())).thenReturn(Optional.of(buildClientImage()));
        when(crudClientMapper.updateClientData(any(ClientServiceObject.class), any(Client.class))).thenReturn(buildClient());
        when(crudClientMapper.updateClientImage(any(ClientServiceObject.class), any(ClientImage.class))).thenReturn(buildClientImage());

        assertEquals(clientServiceObject, crudClientImpl.updateClient("CED", "123456789", clientServiceObject));
    }

    @Test
    void updateClientFailTest() {
        when(crudClientRepository.getById(any(ClientPrimaryKey.class))).thenReturn(null);
        when(crudClientImageRepository.findById(anyString())).thenReturn(Optional.empty());

        assertThrows(NoDataException.class, () -> crudClientImpl.updateClient("CED", "123456789", clientServiceObject));
    }

    @Test
    void deleteClientTest() {
        when(crudClientRepository.getById(any(ClientPrimaryKey.class))).thenReturn(buildClient());
        when(crudClientImageRepository.findById(anyString())).thenReturn(Optional.of(buildClientImage()));

        crudClientImpl.deleteClient("CED", "123456789");

        verify(crudClientRepository, times(1)).delete(any(Client.class));
        verify(crudClientImageRepository, times(1)).delete(any(ClientImage.class));
    }

    @Test
    void deleteClientFailTest() {
        when(crudClientRepository.getById(any(ClientPrimaryKey.class))).thenReturn(null);
        when(crudClientImageRepository.findById(anyString())).thenReturn(Optional.empty());

        assertThrows(ClientNotFoundException.class, () -> crudClientImpl.deleteClient("CED", "123456789"));
    }

    private ClientServiceObject createClientServiceObject() {
        ClientServiceObject clientServiceObject = new ClientServiceObject();
        clientServiceObject.setIdentificationType("CED");
        clientServiceObject.setIdentification("123456789");

        return clientServiceObject;
    }

    private Client buildClient() {
        Client client = new Client();
        client.setAge(25);
        client.setBirthPlace("birthPlace");
        client.setIdentification("123456789");
        client.setIdentificationType("CED");
        client.setLastNames("Perez");
        client.setNames("Juan");

        return client;
    }

    private ClientImage buildClientImage() {
        ClientImage clientImage = new ClientImage();
        clientImage.setId("CED123456789");
        clientImage.setClientPhoto("A very handsome photo of me in base64 ;)");

        return clientImage;
    }
}
