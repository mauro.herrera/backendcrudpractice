package com.pragma.crudbackend.mapper;

import com.pragma.crudbackend.dto.Client;
import com.pragma.crudbackend.dto.ClientImage;
import com.pragma.crudbackend.dto.ClientServiceObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {CrudClientMapper.class})
class CrudClientMapperTest {

    @Autowired
    private CrudClientMapper crudClientMapper;

    @Test
    void mapServiceObjectToClientTest() {
        assertEquals(25, crudClientMapper.mapServiceObjectToClient(createClientServiceObject()).getAge());
    }

    @Test
    void mapServiceObjectToClientImageTest() {
        assertEquals("CED123456789", crudClientMapper.mapServiceObjectToClientImage(createClientServiceObject()).getId());
    }

    @Test
    void mapClientToServiceObjectTest() {
        assertEquals(25, crudClientMapper.mapClientToServiceObject(buildClient(), buildClientImage()).getAge());
    }

    @Test
    void mapClientListToServiceObjectListTest() {
        List<ClientServiceObject> clientServiceObjectList =
                crudClientMapper.mapClientListToServiceObjectList(List.of(buildClient()), List.of(buildClientImage()));

        assertEquals(25, clientServiceObjectList.get(0).getAge());
    }

    @Test
    void updateClientDataTest() {
        assertEquals(25, crudClientMapper.updateClientData(createClientServiceObject(), buildClient()).getAge());
    }

    @Test
    void updateClientImageTest() {
        assertEquals("CED123456789", crudClientMapper.updateClientImage(createClientServiceObject(), buildClientImage()).getId());
    }

    private ClientServiceObject createClientServiceObject() {
        ClientServiceObject clientServiceObject = new ClientServiceObject();
        clientServiceObject.setAge(25);
        clientServiceObject.setBirthPlace("birthPlace");
        clientServiceObject.setIdentification("123456789");
        clientServiceObject.setIdentificationType("CED");
        clientServiceObject.setLastNames("Perez");
        clientServiceObject.setNames("Juan");

        return clientServiceObject;
    }

    private Client buildClient() {
        Client client = new Client();
        client.setAge(25);
        client.setBirthPlace("birthPlace");
        client.setIdentification("123456789");
        client.setIdentificationType("CED");
        client.setLastNames("Perez");
        client.setNames("Juan");

        return client;
    }

    private ClientImage buildClientImage() {
        ClientImage clientImage = new ClientImage();
        clientImage.setId("CED123456789");
        clientImage.setClientPhoto("A very handsome photo of me in base64 ;)");

        return clientImage;
    }
}
