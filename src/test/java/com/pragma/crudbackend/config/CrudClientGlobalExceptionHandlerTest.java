package com.pragma.crudbackend.config;

import com.pragma.crudbackend.config.CrudClientGlobalExceptionHandler;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {CrudClientGlobalExceptionHandler.class})
class CrudClientGlobalExceptionHandlerTest {

    @Autowired
    private CrudClientGlobalExceptionHandler crudClientGlobalExceptionHandler;

    @Test
    void handleCityNotFoundExceptionTest() {
        assertEquals(HttpStatus.NOT_FOUND, crudClientGlobalExceptionHandler.handleClientNotFoundException().getStatusCode());
    }

    @Test
    void handleNoDataFoundExceptionTest() {
        assertEquals(HttpStatus.NOT_FOUND, crudClientGlobalExceptionHandler.handleNoDataFoundException().getStatusCode());
    }

    @Test
    void handleHttpMessageNotReadableExceptionTest() {
        assertEquals(HttpStatus.BAD_REQUEST, crudClientGlobalExceptionHandler.handleHttpMessageNotReadableException().getStatusCode());
    }
}
