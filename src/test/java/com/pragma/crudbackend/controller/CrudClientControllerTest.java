package com.pragma.crudbackend.controller;

import com.pragma.crudbackend.interfaces.ICrudClientImpl;
import com.pragma.crudbackend.dto.ClientServiceObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = { CrudClientController.class})
class CrudClientControllerTest {

    @Autowired
    private CrudClientController crudClientController;

    @MockBean
    private ICrudClientImpl crudClientImpl;

    ClientServiceObject clientServiceObject = createClientServiceObject();

    @Test
    void createClientTest() {
        when(crudClientImpl.createClient(any(ClientServiceObject.class))).thenReturn(clientServiceObject);

        assertEquals(HttpStatus.OK,
                crudClientController.createClient(clientServiceObject).getStatusCode());
    }

    @Test
    void getClientByIdTest() {
        when(crudClientImpl.getClientById(anyString(), anyString())).thenReturn(clientServiceObject);

        assertEquals(HttpStatus.OK,
                crudClientController.getClientById("CED", "123456").getStatusCode());
    }

    @Test
    void getAllClientsTest() {
        List<ClientServiceObject> clientServiceObjects = List.of(createClientServiceObject());

        when(crudClientImpl.getAllClients()).thenReturn(clientServiceObjects);

        assertEquals(HttpStatus.OK,
                crudClientController.getAllClients().getStatusCode());
    }

    @Test
    void updateClientTest() {
        when(crudClientImpl.updateClient(anyString(), anyString(), any(ClientServiceObject.class))).thenReturn(clientServiceObject);

        assertEquals(HttpStatus.OK,
                crudClientController.updateClient("CED", "123456", clientServiceObject).getStatusCode());
    }

    @Test
    void deleteClientTest() {
        doNothing().when(crudClientImpl).deleteClient(anyString(), anyString());

        assertEquals(HttpStatus.OK,
                crudClientController.deleteClient("CED", "123456").getStatusCode());
    }

    private ClientServiceObject createClientServiceObject() {
        ClientServiceObject clientServiceObject = new ClientServiceObject();
        clientServiceObject.setIdentificationType("CED");
        clientServiceObject.setIdentification("123456789");

        return clientServiceObject;
    }
}
