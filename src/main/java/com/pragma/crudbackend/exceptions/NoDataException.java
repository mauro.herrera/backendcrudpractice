package com.pragma.crudbackend.exceptions;

public class NoDataException extends RuntimeException{
    public NoDataException() {
        super("No data found");
    }
}
