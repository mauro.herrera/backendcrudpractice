package com.pragma.crudbackend.controller;

import com.pragma.crudbackend.interfaces.ICrudClientImpl;
import com.pragma.crudbackend.dto.ClientServiceObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class CrudClientController {

    @Autowired
    private ICrudClientImpl crudClientImpl;

    //C
    @PostMapping(path = "/createClient", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientServiceObject> createClient(@RequestBody ClientServiceObject client) {

        return new ResponseEntity<>(crudClientImpl.createClient(client), HttpStatus.OK);
    }

    //R
    @GetMapping(path = "/getClient/{idType}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientServiceObject> getClientById(
            @PathVariable("idType") String idType, @PathVariable("id") String id) {

        return new ResponseEntity<>(crudClientImpl.getClientById(idType, id), HttpStatus.OK);
    }

    @GetMapping(path = "/getClient", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ClientServiceObject>> getAllClients() {

        return new ResponseEntity<>(crudClientImpl.getAllClients(), HttpStatus.OK);
    }

    //U
    @PatchMapping(path = "/updateClient/{idType}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientServiceObject> updateClient(
            @PathVariable("idType") String idType, @PathVariable("id") String id,
            @RequestBody ClientServiceObject client) {

        return new ResponseEntity<>(crudClientImpl.updateClient(idType, id, client), HttpStatus.OK);
    }

    //D
    @DeleteMapping(path = "/deleteClient/{idType}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> deleteClient(
            @PathVariable("idType") String idType, @PathVariable("id") String id) {

        crudClientImpl.deleteClient(idType, id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
