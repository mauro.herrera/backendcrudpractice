package com.pragma.crudbackend.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ClientServiceObject {

    @NotNull
    private String identificationType;
    @NotNull
    private String identification;
    private String names;
    private String lastNames;
    private Integer age;
    private String birthPlace;
    private String clientPhoto;
}
