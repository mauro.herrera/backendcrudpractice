package com.pragma.crudbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@IdClass(ClientPrimaryKey.class)
@Table(name = "cliente", indexes = {@Index(name="SEC_INDEX",columnList = "age")})
public class Client {

    @NotNull
    @Id
    @Column(name = "identification_type")
    private String identificationType;

    @NotNull
    @Id
    @Column(name = "identification")
    private String identification;

    @Column(name = "names")
    private String names;

    @Column(name = "lastnames")
    private String lastNames;

    @NotNull
    @Column(name = "age", nullable = false)
    private Integer age;

    @Column(name = "birthplace")
    private String birthPlace;
}

