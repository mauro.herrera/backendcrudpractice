package com.pragma.crudbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientPrimaryKey implements Serializable {

    private String identificationType;
    private String identification;
}
