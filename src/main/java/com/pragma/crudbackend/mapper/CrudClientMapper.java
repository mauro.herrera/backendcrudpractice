package com.pragma.crudbackend.mapper;

import com.pragma.crudbackend.interfaces.ICrudClientMapper;
import com.pragma.crudbackend.dto.Client;
import com.pragma.crudbackend.dto.ClientImage;
import com.pragma.crudbackend.dto.ClientServiceObject;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CrudClientMapper implements ICrudClientMapper {

    @Override
    public Client mapServiceObjectToClient(ClientServiceObject clientServiceObject) {
        Client client = new Client();
        client.setAge(clientServiceObject.getAge());
        client.setBirthPlace(clientServiceObject.getBirthPlace());
        client.setIdentificationType(clientServiceObject.getIdentificationType());
        client.setIdentification(clientServiceObject.getIdentification());
        client.setLastNames(clientServiceObject.getLastNames());
        client.setNames(clientServiceObject.getNames());

        return client;
    }

    @Override
    public ClientImage mapServiceObjectToClientImage(ClientServiceObject clientServiceObject) {
        ClientImage clientImage = new ClientImage();
        clientImage.setId(clientServiceObject.getIdentificationType().concat(clientServiceObject.getIdentification()));
        clientImage.setClientPhoto(clientServiceObject.getClientPhoto());

        return clientImage;
    }

    @Override
    public ClientServiceObject mapClientToServiceObject(Client client, ClientImage clientImage) {
        ClientServiceObject clientServiceObject = new ClientServiceObject();
        clientServiceObject.setAge(client.getAge());
        clientServiceObject.setBirthPlace(client.getBirthPlace());
        clientServiceObject.setIdentificationType(client.getIdentificationType());
        clientServiceObject.setIdentification(client.getIdentification());
        clientServiceObject.setLastNames(client.getLastNames());
        clientServiceObject.setNames(client.getNames());
        clientServiceObject.setClientPhoto(clientImage.getClientPhoto());

        return clientServiceObject;
    }

    @Override
    public List<ClientServiceObject> mapClientListToServiceObjectList(List<Client> clientList, List<ClientImage> clientImageList) {
        Map<String, String> clientImagesMap = clientImageList.stream().collect(Collectors.toMap(ClientImage::getId, ClientImage::getClientPhoto));
        List<ClientServiceObject> clientServiceObjectList = new ArrayList<>();

        for (Client client : clientList) {
            ClientServiceObject clientServiceObject = new ClientServiceObject();
            clientServiceObject.setAge(client.getAge());
            clientServiceObject.setBirthPlace(client.getBirthPlace());
            clientServiceObject.setIdentificationType(client.getIdentificationType());
            clientServiceObject.setIdentification(client.getIdentification());
            clientServiceObject.setLastNames(client.getLastNames());
            clientServiceObject.setNames(client.getNames());

            clientServiceObject.setClientPhoto(clientImagesMap.get(client.getIdentificationType().concat(client.getIdentification())));

            clientServiceObjectList.add(clientServiceObject);
        }

        return clientServiceObjectList;
    }

    @Override
    public Client updateClientData(ClientServiceObject clientToUpdate, Client dbClient) {
        dbClient.setAge(clientToUpdate.getAge() != null ? clientToUpdate.getAge() : dbClient.getAge());
        dbClient.setBirthPlace(clientToUpdate.getBirthPlace() != null ? clientToUpdate.getBirthPlace() : dbClient.getBirthPlace());
        dbClient.setLastNames(clientToUpdate.getLastNames() != null ? clientToUpdate.getLastNames() : dbClient.getLastNames());
        dbClient.setNames(clientToUpdate.getNames() != null ? clientToUpdate.getNames() : dbClient.getNames());

        return dbClient;
    }

    @Override
    public ClientImage updateClientImage(ClientServiceObject clientToUpdate, ClientImage dbClientImage) {
        dbClientImage.setClientPhoto(clientToUpdate.getClientPhoto() != null ? clientToUpdate.getClientPhoto() : dbClientImage.getClientPhoto());

        return dbClientImage;
    }
}