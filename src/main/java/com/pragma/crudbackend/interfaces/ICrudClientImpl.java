package com.pragma.crudbackend.interfaces;

import com.pragma.crudbackend.dto.ClientServiceObject;

import java.util.List;

public interface ICrudClientImpl {

    ClientServiceObject createClient(ClientServiceObject client);
    ClientServiceObject getClientById(String idType, String id);
    List<ClientServiceObject> getAllClients();
    ClientServiceObject updateClient(String idType, String id, ClientServiceObject client);
    void deleteClient(String idType, String id);
}
