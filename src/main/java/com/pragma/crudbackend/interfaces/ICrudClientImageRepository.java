package com.pragma.crudbackend.interfaces;

import com.pragma.crudbackend.dto.ClientImage;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ICrudClientImageRepository extends MongoRepository<ClientImage, String> {

    @Aggregation(pipeline = { " { '$match': { '_id': { '$in':  ?0  } } }" } )
    List<ClientImage> findByIdsIn(List<String> ids);
}
