package com.pragma.crudbackend.interfaces;

import com.pragma.crudbackend.dto.Client;
import com.pragma.crudbackend.dto.ClientPrimaryKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICrudClientRepository extends JpaRepository<Client, ClientPrimaryKey> {
}
