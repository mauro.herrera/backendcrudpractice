package com.pragma.crudbackend.interfaces;

import com.pragma.crudbackend.dto.Client;
import com.pragma.crudbackend.dto.ClientImage;
import com.pragma.crudbackend.dto.ClientServiceObject;

import java.util.List;

public interface ICrudClientMapper {

    Client mapServiceObjectToClient(ClientServiceObject clientServiceObject);
    ClientImage mapServiceObjectToClientImage(ClientServiceObject clientServiceObject);
    ClientServiceObject mapClientToServiceObject(Client client, ClientImage clientImage);
    List<ClientServiceObject> mapClientListToServiceObjectList(List<Client> clientList, List<ClientImage> clientImageList);

    Client updateClientData(ClientServiceObject clientToUpdate, Client dbClient);
    ClientImage updateClientImage(ClientServiceObject clientToUpdate, ClientImage dbClientImage);
}
