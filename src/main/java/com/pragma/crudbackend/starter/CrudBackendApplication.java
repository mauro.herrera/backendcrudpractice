package com.pragma.crudbackend.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@ComponentScan("com.pragma.crudbackend.*")
@EnableJpaRepositories("com.pragma.crudbackend.interfaces")
@EntityScan("com.pragma.crudbackend.dto")
@EnableMongoRepositories("com.pragma.crudbackend.interfaces")
public class CrudBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrudBackendApplication.class, args);
    }
}
