package com.pragma.crudbackend.impl;

import com.pragma.crudbackend.exceptions.ClientNotFoundException;
import com.pragma.crudbackend.exceptions.NoDataException;
import com.pragma.crudbackend.interfaces.ICrudClientImageRepository;
import com.pragma.crudbackend.interfaces.ICrudClientImpl;
import com.pragma.crudbackend.interfaces.ICrudClientMapper;
import com.pragma.crudbackend.interfaces.ICrudClientRepository;
import com.pragma.crudbackend.dto.Client;
import com.pragma.crudbackend.dto.ClientImage;
import com.pragma.crudbackend.dto.ClientPrimaryKey;
import com.pragma.crudbackend.dto.ClientServiceObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CrudClientImpl implements ICrudClientImpl {

    @Autowired
    private ICrudClientRepository crudClientRepository;

    @Autowired
    private ICrudClientImageRepository crudClientImageRepository;

    @Autowired
    private ICrudClientMapper crudClientMapper;

    @Override
    public ClientServiceObject createClient(ClientServiceObject client) {
        Client clientData = crudClientMapper.mapServiceObjectToClient(client);
        ClientImage clientImage = crudClientMapper.mapServiceObjectToClientImage(client);

        crudClientRepository.save(clientData);
        crudClientImageRepository.save(clientImage);

        log.info("Client created successfully...");

        return client;
    }

    @Override
    public ClientServiceObject getClientById(String idType, String id) {

        Client clientData = crudClientRepository.findById(new ClientPrimaryKey(idType, id)).orElse(null);
        ClientImage clientImage = crudClientImageRepository.findById(idType.concat(id)).orElse(null);

        ClientServiceObject clientServiceObject;

        if (clientData != null && clientImage != null) {
            clientServiceObject = crudClientMapper.mapClientToServiceObject(clientData, clientImage);
            log.info("Client found successfully...");
        } else {
            log.info("No client found for client id: {}", id);
            throw new ClientNotFoundException(id);
        }

        return clientServiceObject;
    }

    @Override
    public List<ClientServiceObject> getAllClients() {
        List<Client> clients = crudClientRepository.findAll();

        List<String> clientsIds = clients.stream().map(client -> client.getIdentificationType()+client.getIdentification())
                .collect(Collectors.toList());

        List<ClientImage> clientsImages = crudClientImageRepository.findByIdsIn(clientsIds);

        //List<ClientImage> clientsImages = crudClientImageRepository.findAll();

        return crudClientMapper.mapClientListToServiceObjectList(clients, clientsImages);
    }

    @Override
    public ClientServiceObject updateClient(String idType, String id, ClientServiceObject client) {

        Client clientData = crudClientRepository.getById(new ClientPrimaryKey(idType, id));
        ClientImage clientImage = crudClientImageRepository.findById(idType.concat(id)).orElse(null);

        if (clientImage != null && clientData.getIdentificationType().equals(idType) && clientData.getIdentification().equals(id)) {
            crudClientRepository.save(crudClientMapper.updateClientData(client, clientData));
            crudClientImageRepository.save(crudClientMapper.updateClientImage(client, clientImage));

            log.info("Client updated successfully...");
        } else {
            log.info("No data found for client id: {}", id);
            throw new NoDataException();
        }

        return client;
    }

    @Override
    public void deleteClient(String idType, String id) {

        Client clientData = crudClientRepository.getById(new ClientPrimaryKey(idType, id));
        ClientImage clientImage = crudClientImageRepository.findById(idType.concat(id)).orElse(null);

        if (clientImage != null && clientData.getIdentificationType().equals(idType) && clientData.getIdentification().equals(id)) {
            crudClientRepository.delete(clientData);
            crudClientImageRepository.delete(clientImage);

            log.info("Client deleted successfully...");
        } else {
            log.info("No data found for client id: {}", id);
            throw new ClientNotFoundException(id);
        }
    }
}
